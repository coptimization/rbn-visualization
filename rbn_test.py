from graphics import *
import time
import random
import math
import subprocess

# Test parameters
num_population = 50
num_steps = 20
num_sim = 5

# RBN parameters
num_nodes = 8
num_boolfunction = num_nodes - 1
bf_maxvalue = 5
bf_minvalue = 0
min_connections = 3

# Path to files
rbn_parameters_path = "parameters_rbn.txt"
rbn_run_path = "gcc run_rbn.c -o run_rbn -lm;./run_rbn"
rbn_results_path = "results_rbn.txt"

# Draw parameters
frequency = 0.4
height = 700
width = 1000
start_point = Point(450, 350)
vertical_step = 20

# Class
class RBN:
    def __init__(self, connections, boolfunctions):
        self.connections = connections
        self.boolfunctions = boolfunctions
        self.values = []
        self.angle_values = []
        self.step_lenght_values = []

    def setValues(self, n_values):
        self.values.append(n_values)

    def getValues(self, step):
        return self.values[step]
    
    def deleteValues(self):
        self.values = []
        self.angle_values = []
        self.step_lenght_values = []

    def calculateTrajectory(self):
        for i in self.values:
            result = i.split(" ")
            self.angle_values.append(float(result[0]))
            self.step_lenght_values.append(int(result[1]))

# Functions
def createPopulation():
    population = []
    for i in range(num_population):
        connections = []
        booleanfunctions = []
        for j in range(num_nodes):
            connections.append([])
            k_cont = 1
            for k in range(num_nodes):
                k_rand = random.randint(0,2)
                if (k_rand == 0):
                    if (k_cont > (num_nodes - min_connections)):
                        k_rand = random.randint(1,2)
                    k_cont += 1
                connections[j].append(k_rand)
        for j in range(num_nodes):
            booleanfunctions.append([])
            for k in range(num_boolfunction):
                booleanfunctions[j].append(random.randint(bf_minvalue, bf_maxvalue))
        individual = RBN(connections, booleanfunctions)
        population.append(individual)
    return population

def createInitialState():
    initial_states = []
    first_state = [0]*num_nodes
    second_state = [1]*num_nodes
    initial_states.append(first_state)
    initial_states.append(second_state)
    for i in range(num_sim - 2):
        init_state = []
        for j in range(num_nodes):
            init_state.append(random.randint(0,1))
        initial_states.append(init_state)
    return initial_states

def runRBN(rbn, init_state):
    print("Py - Calling RBN test")
    while True:
        with open(rbn_parameters_path, "w+") as parameters:
            try:
                parameters.truncate(0)
                parameters.write("%d\n" % num_nodes)
                parameters.write("%d\n" % num_steps)
                for i in range(num_nodes):
                    for j in range(num_nodes):
                        parameters.write("%d" % rbn.connections[i][j])
                    parameters.write("\n")
                for i in range(num_nodes):
                    for j in range(num_boolfunction):
                        parameters.write("%d" % rbn.boolfunctions[i][j])
                    parameters.write("\n")
                for i in range(num_nodes):
                    parameters.write("%d" % init_state[i])
                parameters.close()
                break
            except:
                print("Py - Couldnt open parameters file")
    called_rbn = False
    while not called_rbn:
        try:
            s = subprocess.check_call(rbn_run_path, shell=True)
            called_rbn = True
        except:
            print("Py - Couldnt call RBN test")
            time.sleep(1)

def getResults(rbn):
    finish = False
    while not finish:
        if os.path.getsize(rbn_results_path) != 0:
            finish = True
        time.sleep(0.2)

    read = False
    while not read:
        with open(rbn_results_path, "r+") as results_file:
            try:
                for i in range(num_steps):
                    rbn.setValues(results_file.readline())
                results_file.truncate(0)
                results_file.close()
                read = True
            except:
                print("Py - Couldnt read the results")
                time.sleep(1)

def drawResults(rbn, init_state):
    step = 0
    robot_angle = 0
    initial_state_text = Text(Point(850, 650), str(init_state))
    initial_state_text.draw(win)
    values_text = []
    rbn_trajectory = []
    step_text = Text(Point(75, 680), str(step))
    rbn.calculateTrajectory()
    time_before = time.time()
    while (step < num_steps):
        if (time.time() - time_before > frequency):
            if (step == 0):
                first_point = start_point
            else:
                first_point = rbn_trajectory[-1].getP2()
            robot_angle += rbn.angle_values[step]
            second_point = Point(first_point.getX() + 3 *(rbn.step_lenght_values[step] *math.cos(robot_angle)), 
                first_point.getY() + 3 *(rbn.step_lenght_values[step] *math.sin(robot_angle)))
            rbn_trajectory.append(Line(first_point, second_point))
            values_text.append(Text(Point(40, 620 - (vertical_step *step)), rbn.getValues(step)))
            step_text.setText(str(step + 1))
            step_text.undraw()
            rbn_trajectory[step].draw(win)
            values_text[step].draw(win)
            step_text.draw(win)
            time_before = time.time()
            step += 1
    win.getMouse()

    for i in values_text:
        i.undraw()
    for i in rbn_trajectory:
        i.undraw()
    step_text.undraw()
    initial_state_text.undraw()
    rbn.deleteValues()


# Windows parameters
win = GraphWin("RBN Test", width, height, autoflush=True)
win.setBackground("white")
win.setCoords(0, 0, width, height)
step_number_text = Text(Point(25, 680), "Step: ")
rbn_values_text = Text(Point(75, 650), "Angle - Step Lenght")
step_number_text.draw(win)
rbn_values_text.draw(win)

population = createPopulation()
for rbn in population:
    initial_states = createInitialState()
    for init_state in initial_states:
        runRBN(rbn, init_state)
        getResults(rbn)
        drawResults(rbn, init_state)
win.close()