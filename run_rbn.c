#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <float.h>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif
#define M2_PI (2.0*M_PI)

char *parameters_file = "parameters_rbn.txt";
char *results_file = "results_rbn.txt";

/* Set Random Boolean Network parameters*/
int num_nodes;
int num_boolfuntcions;
int max_steps;

/* Creating Random Boolean Network struct */
struct RBN{
	int *nodes_state;
    int *connections_number;
	int **connections;
	int **booleanfunctions;
};
struct RBN individual_rbn;

void printInitialStates(){
    for(int i = 0; i < num_nodes; i++)
        printf("%d", individual_rbn.nodes_state[i]);
    printf("\n");
}

/*-------------------------------------------------------------------*/
/* Random Boolean Network functions                                  */
/*-------------------------------------------------------------------*/

void allocateRBN(){
    individual_rbn.nodes_state = malloc(sizeof(int)*num_nodes);
    individual_rbn.connections_number = malloc(sizeof(int)*num_nodes);
    individual_rbn.connections = malloc(sizeof(int*)*num_nodes);
	for(int i = 0; i < num_nodes; i++)
		individual_rbn.connections[i] = malloc(sizeof(int)*num_nodes);
    individual_rbn.booleanfunctions = malloc(sizeof(int*)*num_nodes);
	for(int i = 0; i < num_nodes; i++)
		individual_rbn.booleanfunctions[i] = malloc(sizeof(int)*num_boolfuntcions);
}

void calculateConnectionsNum(){
    // Calculate the number of connections each node has
	for(int i = 0; i < num_nodes; i++){
		individual_rbn.connections_number[i] = 0;
		for(int j = 0; j < num_nodes; j++){
			if(individual_rbn.connections[i][j] > 0)
				individual_rbn.connections_number[i]++;
		}
    }
}

void setRBNparameters(){
	// Open and read the parameters file
	char ch;
	FILE *fptr = fopen(parameters_file, "r");
	if(fptr == NULL){
			printf("C - Cannot open parameters_rbn.txt file\n");
			exit(0);
	}
    else{
        fseek(fptr, 0, SEEK_END); 
	    long fsize = ftell(fptr);
        if (fsize == 0){
            printf("C - File is empty");
            exit(0);
        }
    }
    fseek(fptr, 0, SEEK_SET);
    num_nodes = (int)fgetc(fptr) - '0';
    ch = fgetc(fptr);
    if (ch != '\n'){
        num_nodes = num_nodes*10 + ((int)ch - '0');
        ch = fgetc(fptr);
    }
    num_boolfuntcions = num_nodes - 1;
    max_steps = (int)fgetc(fptr) - '0';
    ch = fgetc(fptr);
    if (ch != '\n'){
        max_steps = max_steps*10 + ((int)ch - '0');
        ch = fgetc(fptr);
    }
    allocateRBN();
    printf("C - Num. nodes = %d and max. steps = %d\n", num_nodes, max_steps);
    // Set the connections value, booleanfuntions value and nodes state
	for(int i = 0; i < num_nodes; i++){
		for(int j = 0; j < num_nodes; j++){
			ch = fgetc(fptr);
			if(ch != '\n')
				individual_rbn.connections[i][j] = (int)ch - '0';
			else
				j = -1;
		}
	}
	for(int i = 0; i < num_nodes; i++){
		for(int j = 0; j < num_boolfuntcions; j++){
			ch = fgetc(fptr);
			if(ch != '\n')
				individual_rbn.booleanfunctions[i][j] = (int)ch - '0';
			else
				j = -1;
		}
	}
    for(int i = 0; i < num_nodes; i++){
        ch = fgetc(fptr);
        if (ch != '\n')
		    individual_rbn.nodes_state[i] = (int)ch - '0';
        else
            i = -1;
    }
    printInitialStates();
    calculateConnectionsNum();
	fclose(fptr);
}

void freeingRBN(){
	for(int i = 0; i < num_nodes; i++)
		free(individual_rbn.connections[i]);
	for(int i = 0; i < num_boolfuntcions; i++)
		free(individual_rbn.booleanfunctions[i]);
	free(individual_rbn.nodes_state);
    free(individual_rbn.connections_number);
	free(individual_rbn.booleanfunctions);
    free(individual_rbn.connections);
}

int logicmapRBN(int boolfunction, int node_a, int node_b){
	switch(boolfunction){
		case 0:
			return node_a & node_b; /*AND*/
		case 1:
			return node_a | node_b; /*OR*/
		case 2:
			return node_a ^ node_b; /*XOR*/
		case 3:
			return !(node_a ^ node_b); /*XNOR*/
		case 4:
			return !(node_a & node_b); /*NAND*/
		case 5:
			return !(node_a | node_b); /*NOR*/
		default:
			printf("C - Error in logicmap = %d\n", boolfunction);
			break;
	}
	return 0;
}

void calculateRbnNextStep(){
    // Calculate the next step for each node
    int* new_node_states = malloc(sizeof(int)*num_nodes);
    for(int i = 0; i < num_nodes; i++){
        // Get the state of each node connected to itself
        if(individual_rbn.connections_number[i] > 1){
            int* connected_nodes_state = malloc(sizeof(int)*num_nodes);
            int cont_nodes = 0;
            for(int j = 0; j < num_nodes; j++){
                if(individual_rbn.connections[i][j] == 1){
                    connected_nodes_state[cont_nodes] = individual_rbn.nodes_state[j];
                    cont_nodes++;
                }
                else if(individual_rbn.connections[i][j] == 2){
                    if(individual_rbn.nodes_state[j] == 0)
                        connected_nodes_state[cont_nodes] = 1;
                    else 
                        connected_nodes_state[cont_nodes] = 0;
                    cont_nodes++;
                }
            }
            // Calculate next node state
            int flogic_num = individual_rbn.connections_number[i] - 1;
            int *connected_state = malloc(sizeof(int)*(individual_rbn.connections_number[i] + flogic_num));
            for(int j = 0; j < individual_rbn.connections_number[i]; j++)
                connected_state[j] = connected_nodes_state[j];
            int cont_states = 0;
            for(int j = 0; j < flogic_num; j++){
                int aux_flogic = individual_rbn.booleanfunctions[i][j];
                int new_state = logicmapRBN(aux_flogic, connected_state[cont_states], connected_state[cont_states+1]);
                connected_state[individual_rbn.connections_number[i] + j] = new_state;
                cont_states += 2;
            }
            new_node_states[i] = connected_state[cont_states-1];
            free(connected_nodes_state);
            free(connected_state);
        }
        else
            new_node_states[i] = individual_rbn.nodes_state[i];
    }
    // Set the new states for each node
    for(int i = 0; i < num_nodes; i++)
        individual_rbn.nodes_state[i] = new_node_states[i];
    free(new_node_states);
}

void getValues(double *angle, int *step_lenght){
    // Get the angle in rad
    int angle_direction = 0;
    int angle_num = num_nodes/2;
    int num_directions = angle_num*angle_num;
    for(int i = 0; i < angle_num; i++){
        if(individual_rbn.nodes_state[i] == 1)
            angle_direction += pow(2, i);
    }
    *angle = (angle_direction/(float)num_directions)*M2_PI;
    *angle = *angle * (180/M_PI);
    // Get the step lenght (distance it goes forward)
    *step_lenght = 0;
    for(int i = angle_num; i < num_nodes; i++){
        if(individual_rbn.nodes_state[i] == 1)
            *step_lenght += pow(2, i - angle_num);
    }
}

void looping(){
    double angle = 0;
    int step_lenght = 0;
    FILE *fptr = fopen(results_file, "w");
    for (int step = 0; step < max_steps; step++){
        getValues(&angle, &step_lenght);
        fprintf(fptr, "%.2f %d\n", angle, step_lenght);
        calculateRbnNextStep();
    }
    fclose(fptr);
}

int main(){
    printf("C - Running RBN test\n");
    setRBNparameters();
    looping();
    freeingRBN();
    printf("C - Run finished\n");
    return 0;
}